import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage

} from 'react-native';

class PaginaInicial extends React.Component{
    constructor(props){
        super(props)
        
    }
    render(){
        return(
            <View style={styles.viewPrincipal}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Cadastro')}>
                    <Text style={styles.linkPaginas}>Cadastrar um Produto</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VerProdutos')}>
                    <Text style={styles.linkPaginas}>Ver Produtos</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewPrincipal: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: "#f0c9c9"
    },
    linkPaginas:{
        marginTop: 30,
        fontSize: 30,

    }
});

export default PaginaInicial;