import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage

} from 'react-native';

import { Input } from 'react-native-elements';


class CadastrarProduto extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            nomeProduto: '',
            valorProduto: 0,
            quantidadeProduto: 0,
            resposta: ""
        }
    }

    handleNome = (inputNomeProduto) => {
        this.setState({
            nomeProduto: inputNomeProduto
        });
    }

    handleValor = (inputValorProduto) => {
        this.setState({
            valorProduto: inputValorProduto
        });
    }

    handlePress = async () => {
        //Ajeitar virgula depois
        let valor = parseFloat(this.state.valorProduto)
        if(this.state.nomeProduto === ""){
            this.setState({resposta: "Adicione um nome ao produto"});
            this.render();
        }else if(this.state.valor === 0){
            this.setState({resposta: "Adicione um preço ao produto"});
            this.render();
        }else if(this.state.valor < 0){
            this.setState({resposta: "Preço não pode ser negativo"});
            this.render();
        }else{
            let estoqueAntigo = await this.props.functions.returnEstoque();
            //Inicializando estoque vazio, pela primeira vez
            if(estoqueAntigo == undefined){
                let estoqueVazio = {produtos: []}
                this.props.functions.storeEstoque(estoqueVazio);
                //Tentando novamente
                estoqueAntigo = await this.props.functions.returnEstoque();
            }
            
            if(estoqueAntigo === "error"){
                this.setState({resposta: "Algum erro aconteceu"})
            }else{
                let listaProdutos = estoqueAntigo.produtos;

                let nomeIgual = false;
                listaProdutos.forEach(element => {
                    //Impossibilitar criação de nomes iguais
                    if(element.nome == this.state.nomeProduto){
                        nomeIgual = true;
                    } 
                });

                if(nomeIgual){
                    this.setState({resposta: "Já Existe um produto com esse nome"})
                }else{
                    let novoProduto = {
                        nome: this.state.nomeProduto,
                        valor: this.state.valorProduto,
                        quantidade: 0
                    }

                    estoqueAntigo.produtos.push(novoProduto)

                    //Atualizar AsyncStorage com novo Estoque
                    this.props.functions.storeEstoque(estoqueAntigo);
                    //Atualizar o estado do estoque
                    this.props.functions.atualizarEstoque();

                    this.setState({resposta: `
                        Produto
                        Nome: ${this.state.nomeProduto}
                        Preço: ${this.state.valorProduto}
                        Cadastrado
                    `})
                }

            }

            this.render();
        }
        
    }

    render(){
        return(
            <View style={styles.viewPrincipal}>
                <Text style={styles.tituloCadastrarProduto}>Cadastre um produto</Text>
                <Input inputStyle={styles.inputCadastro} ref="inputNomeProduto" onChangeText={this.handleNome} placeholder='Nome do Produto'/>
                <Input inputStyle={styles.inputCadastro} keyboardType='numeric' ref="inputValorProduto" onChangeText={this.handleValor} placeholder='Preço do Produto'/>
                <TouchableOpacity style={styles.buttonSubmit} onPress={this.handlePress}>
                    <Text>Cadastre</Text>
                </TouchableOpacity>
                <Text style={styles.resposta}>{this.state.resposta}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  viewPrincipal: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: "#ebe134"
  },

  tituloCadastrarProduto: {
      marginTop: 30,
      fontSize: 30,
      color: '#ba2b2b'
  },

  buttonSubmit: {
    marginTop: 30,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 20
  },

  resposta: {
    marginTop: 100,
    fontSize: 20,
    color: '#ba2b2b'
  },

  inputCadastro: {
      marginTop: 30
  }
});

export default CadastrarProduto;