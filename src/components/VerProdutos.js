import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
  FlatList

} from 'react-native';



function Produto({ nome, quantidade, valor }){
    return(
        <View styles={styles.viewProduto}>
            <Text style={styles.textoProdutos}>Nome: {nome}  Preço: {valor}  Quantidade: {quantidade}</Text>
        </View>
    );
}

class VerProdutos extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            listaProdutos: [],
            vazio: true
        }
    }

    componentDidMount = async () => {
        this.props.functions.atualizarEstoque();
        let estoque = await this.props.functions.returnEstoque();
        let novaLista = estoque.produtos

        this.setState({listaProdutos: novaLista, vazio: false});

        this.render();
    }
    render(){
        if(this.state.vazio){
            return(
                <SafeAreaView>

                </SafeAreaView>
            )
        }else{
            return(
                <SafeAreaView style={styles.viewPrincipal}>
                    <FlatList
                        data={this.state.listaProdutos} 
                        renderItem={({ item }) => (
                            <Produto 
                                nome={item.nome}
                                quantidade={item.quantidade}
                                valor={item.valor}
                            />
                        )}
                        keyExtractor={item => item.nome}
                    />
                </SafeAreaView>
            )
        }
    }
}

const styles = StyleSheet.create({
    viewPrincipal: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: "#f0c9c9"
    },
    textoProdutos:{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
        fontSize: 15,
    }
});

export default VerProdutos;