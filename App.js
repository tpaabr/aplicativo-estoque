import 'react-native-gesture-handler';
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import CadastrarProduto from './src/components/CadastrarProduto';
import PaginaInicial from './src/components/PaginaInicial';
import VerProdutos from './src/components/VerProdutos';  

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage

} from 'react-native';

const Stack = createStackNavigator();

class App extends React.Component{

  constructor(props){
    super(props)

    this.state = {
      estoque: {
        produtos: []
      }
    }
  }

  //Atualiza Estoque no começo 
  componentDidMount = async () => {
    this.atualizarEstoque();
  }

  atualizarEstoque = async () => {
    let novoEstoque = await this.returnEstoque();
    this.setState({estoque: novoEstoque});
  }

  //Retorna valor de Estoque para ser atualizado como objeto
  returnEstoque = async () => {
    try {
      const value = await AsyncStorage.getItem('@EstoqueProdutos');
      let estoqueAtualizado = JSON.parse(value);
      if (value !== null) {
        // We have data!!
        return estoqueAtualizado  
      }
    } catch (error) {
      return "error"
      console.warn(error);
    }
  };

  storeEstoque = async (novoEstoque) => {
    try {
      let objetoEstoque = novoEstoque;
      await AsyncStorage.setItem('@EstoqueProdutos', JSON.stringify(objetoEstoque));
    } catch (error) {
      console.warn(error);
    }
  };

  cadastrarProdutoEstoque = () => {

  }

  render(){
    return(
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Inicio" component={PaginaInicial} />
        <Stack.Screen name="Cadastro" >
        {
            //Passar props adicionais
            props => <CadastrarProduto {...props} functions={{
              atualizarEstoque: this.atualizarEstoque, 
              returnEstoque: this.returnEstoque, 
              storeEstoque: this.storeEstoque
            }
          } />
        }
        </Stack.Screen>
        <Stack.Screen name="VerProdutos">
          {
            //Passar props adicionais
            props => <VerProdutos {...props} functions={{
                atualizarEstoque: this.atualizarEstoque, 
                returnEstoque: this.returnEstoque, 
                storeEstoque: this.storeEstoque
              }
            } />
          }
        </Stack.Screen>

      </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({
  
});

export default App;
